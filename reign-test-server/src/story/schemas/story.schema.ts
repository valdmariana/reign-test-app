import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { softDeletePlugin } from 'soft-delete-plugin-mongoose';

export type StoryDocument = Story & Document;

@Schema()
export class Story {
    @Prop()
    story_id: string;
    @Prop()
    story_title: string;
    @Prop()
    title: string;
    @Prop()
    story_url: string;
    @Prop()
    url: string;
    @Prop()
    author: string;
    @Prop()
    created_at: string;
}

export const StorySchema = SchemaFactory.createForClass(Story).plugin(softDeletePlugin);