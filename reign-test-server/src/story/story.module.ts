import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StoriesController } from './controllers/stories/stories.controller';
import { Story, StorySchema } from './schemas/story.schema';
import { StoriesService } from './services/stories/stories.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Story.name, schema: StorySchema }]),
        HttpModule
    ],
    controllers: [StoriesController],
    providers: [StoriesService],
})
export class StoryModule { }
