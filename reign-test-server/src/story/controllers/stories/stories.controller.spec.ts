import { Test, TestingModule } from '@nestjs/testing';
import { StoriesController } from './stories.controller';
import { StoriesService } from '../../services/stories/stories.service';
import { StoryModule } from '../../../../../reign-test-app/src/app/story/story.module';
import { StoryService } from '../../../../../reign-test-app/src/app/story/services/story.service';
import { HttpModule } from '@nestjs/common';

describe('StoriesController', () => {
  let controller: StoriesController;
  let service: StoriesService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StoriesController],
      imports: [StoryModule, HttpModule],
      providers: [StoryService]
    }).compile();

    controller = module.get<StoriesController>(StoriesController);
    service = module.get<StoriesService>(StoriesService)
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
