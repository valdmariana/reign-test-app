import { Controller, Delete, Get, Param } from '@nestjs/common';
import { StoriesService } from '../../services/stories/stories.service';

@Controller('stories')
export class StoriesController {

    constructor(public storyService: StoriesService) { }

    @Get()
    async getStories() {
        const stories = await this.storyService.getAllStories()
        return {
            stories
        }
    }

    @Delete(':id')
    async deleteStory(@Param('id') id: string) {
        await this.storyService.deleteStory(id);
        const deletedStory = await this.storyService.findById(id)
        return {
            story: deletedStory
        }
    }
}

