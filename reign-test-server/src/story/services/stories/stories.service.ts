import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import { StoryDocument, Story } from '../../schemas/story.schema';
import { StoryDto } from '../../entities/story.entity';
import { Cron, CronExpression } from '@nestjs/schedule';

@Injectable()
export class StoriesService {

    constructor(private httpService: HttpService,
        @InjectModel(Story.name) private storyModel: SoftDeleteModel<StoryDocument>) {
    }

    @Cron(CronExpression.EVERY_HOUR)
    public async saveStoriesFromApi() {
        const baseUrl = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
        const response = await this.httpService.get(baseUrl).toPromise();
        this.saveStories(response.data.hits);
        const totalPages = response.data.nbPages;
        for (let page = 2; page <= totalPages; page++) {
            let response = await this.httpService.get(baseUrl + `&page=${page}`).toPromise();
            let data = response.data.hits;
            this.saveStories(data);
        }
    }

    public saveStories(stories: StoryDto[]) {
        stories.forEach(async story => {
            let storyFound = await this.findOneStory(story.story_id);
            if (!storyFound) {
                let createdStory = new this.storyModel(story);
                createdStory.save();
            }
        });
    }

    public async findOneStory(storyId: string): Promise<StoryDocument> {
        const storyDeleted = await this.storyModel.findOne({ story_id: storyId, isDeleted: true }).exec();
        if (!storyDeleted) {
            const storyFound = await this.storyModel.findOne({ story_id: storyId, isDeleted: false }).exec();
            return storyFound;
        } else {
            return storyDeleted;
        }
    }

    public async findById(id: string): Promise<StoryDocument> {
        return await this.storyModel.findById(id).exec();
    }

    public async getAllStories() {
        return await this.storyModel.find().sort({ created_at: -1 }).exec()
    }

    public async deleteStory(id: string) {
        return await this.storyModel.softDelete({ _id: id });
    }
}
