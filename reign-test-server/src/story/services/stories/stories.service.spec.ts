import { Test, TestingModule } from '@nestjs/testing';
import { StoriesService } from './stories.service';
import { HttpModule } from '@nestjs/common';
import { Story, StoryDocument } from '../../schemas/story.schema';
import { Model } from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';

describe('StoriesService', () => {
  let service: StoriesService;
  let mockStoryModel: Model<StoryDocument>;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StoriesService,
        {
          provide: getModelToken(Story.name),
          useValue: Model
        }],
      imports: [HttpModule]
    }).compile();

    service = module.get<StoriesService>(StoriesService);
    mockStoryModel = module.get<Model<StoryDocument>>(getModelToken(Story.name));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
