export class StoryDto {
    story_id: string;
    story_title: string;
    title: string;
    story_url: string;
    url: string;
    author: string;
    created_at: string;
}

