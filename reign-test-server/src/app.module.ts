import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StoryModule } from './story/story.module';

@Module({
  imports: [
    StoryModule,
    ScheduleModule.forRoot(),
    MongooseModule.forRoot('mongodb+srv://marivaldr:ePM4pEegOIJCHHZA@reigncluster.ewv3i.mongodb.net/reign-stories?retryWrites=true&w=majority')
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
