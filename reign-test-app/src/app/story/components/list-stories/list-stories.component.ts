import { Component, OnInit } from '@angular/core';
import { StoryService } from '../../services/story.service';
import { Story } from '../../models/story';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-stories',
  templateUrl: './list-stories.component.html',
  styleUrls: ['./list-stories.component.css']
})
export class ListStoriesComponent implements OnInit {
  stories: Story[] = [];
  constructor(public _storyService: StoryService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getAllStories()
  }

  getAllStories() {
    this._storyService.getStories().subscribe((data: any) => {
      this.stories = data.stories;
    }, (err: any) => {
      this.toastr.error('We have not been able to list the stories', 'Error');
    });
  }

  deleteStory(id: string) {
    event?.stopPropagation()
    event?.preventDefault()
    this._storyService.deleteStory(id).subscribe((data: any) => {
      const story = data.story;
      this.toastr.success(`Post with title '${story.story_title ? story.story_title : story.title}' has been deleted`, 'Success');
      this.stories = this.stories.filter((story) => story._id != id)
    }, (err: any) => {
      this.toastr.error('We have not been able to delete the post', 'Error');
    });
  }

  goToUrl(story_url: string, url: string) {
    if (story_url || url) {
      window.open(story_url ? story_url : url, "_BLANK")
    }
  }

}
