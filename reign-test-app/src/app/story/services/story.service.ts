import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StoryData, Story } from '../models/story';

@Injectable({
  providedIn: 'root'
})
export class StoryService {

  constructor(@Inject(HttpClient) http: HttpClient) {
    this.http = http;
  }
  private http: HttpClient;
  private options: any = {
    headers: new HttpHeaders({
      "Accept": "application/json"
    })
  }
  getStories() {
    return this.http.request<StoryData>('get', 'http://localhost:3000/stories', this.options);
  }

  deleteStory(id: string) {
    return this.http.request('delete', 'http://localhost:3000/stories/' + id, this.options);
  }
}
