import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListStoriesComponent } from './components/list-stories/list-stories.component';
import { SharedModule } from '../shared/shared.module';
import { StoryRoutingModule } from './story.routing';
import { StoryService } from './services/story.service';
@NgModule({
  declarations: [ListStoriesComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    SharedModule,
    StoryRoutingModule,
  ],
  providers: [StoryService]

})
export class StoryModule { }
