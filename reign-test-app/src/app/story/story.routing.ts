import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListStoriesComponent } from './components/list-stories/list-stories.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: "list-stories" },
    { path: 'list-stories', component: ListStoriesComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class StoryRoutingModule {
}
