export interface Story {
    _id: string;
    story_id: string;
    story_title: string;
    title: string;
    story_url: string;
    url: string;
    author: string;
    created_at: string;
}

export interface StoryData {
    stories: Story[];
}