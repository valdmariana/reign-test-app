import { Pipe, PipeTransform, Injectable } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatDate'
})
@Injectable({
  providedIn: 'root'
})
export class FormatDatePipe implements PipeTransform {

  transform(datePost: string): string {
    const today = new Date();
    let date = new Date(datePost);
    let time = today.getTime() - date.getTime();
    let days = Math.floor(time / (1000 * 3600 * 24));
    if (days == 0) {
      return moment(date).format('LT');;
    } else if (days == 1) {
      return 'Yesterday'
    } else {
      return moment(date).format('MMM DD')
    }
  }

}
