import { Pipe, PipeTransform, Injectable } from '@angular/core';
import { Story } from '../../story/models/story';

@Pipe({
  name: 'filterStories'
})
@Injectable({
  providedIn: 'root'
})
export class FilterStoriesPipe implements PipeTransform {

  transform(stories: Story[]): Story[] {
    const filteredStories = stories.filter((story) => story.story_title || story.title);
    return filteredStories;
  }

}
