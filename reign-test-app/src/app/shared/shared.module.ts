import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormatDatePipe } from './pipes/format-date.pipe';
import { HeaderComponent } from './components/header/header.component';
import { FilterStoriesPipe } from './pipes/filter-stories.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';



@NgModule({
  declarations: [FormatDatePipe, HeaderComponent, FilterStoriesPipe],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  exports: [
    RouterModule,
    HttpClientModule,
    FormatDatePipe,
    FilterStoriesPipe,
    HeaderComponent,
    BrowserAnimationsModule,
    ToastrModule
  ],

})
export class SharedModule { }
