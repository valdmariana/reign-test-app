# Reign test

Reign test is a web app that lists the story posts fetching data from an external API.

## Prerequisites
- Node v14.0.0 +
- Npm 6.14.8
- Angular cli 11.0.2
- NestJS 7.6.0

## Installation

### Reign test server and Reign test app
You must go to the corresponding folders and in each one execute

```bash
npm install
```

## Execution

### Reign test server

```bash
npm run start
```

### Reign test app

```bash
ng serve
```

## License
[MIT](https://choosealicense.com/licenses/mit/)